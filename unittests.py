import unittest
import main

class UnitTests(unittest.TestCase):
  def testUnobfuscatePassword(self):
    result = main.unobfuscatePassword("cxkahHvQZc4TajwX")
    self.assertTrue(result == "HvcQZxc4kTaajwhX")

  def testDecryptAESEncryptedMessage(self):
    message = "1ma/R67aAv5/a1RhKZfwhpFingaREa/lkGAV4PRXI/lZPLLQ3pGSoDGLz5xRRxZ+YhhTKelWd5RXGf4PVE/uOQ=="
    key = "eC9UDz6zYZ3gJaxa"
    result = main.decryptAESEncryptedMessage(message, key)
    self.assertTrue(result == "http://example.com/?r=gakjnv&utm_source=test")

  def testDecryptArray(self):
    array = [
        "kh4pOGsXY1cWiLsePjrpdbS9ZED7NKbzBiQ8GX9VrBg=",
    "sRzq2do3ANdxLaDvDO31FoHYPNnCBemJiwzxgrkhY7k=",
    "BUm/oY3Pi7yh2igwLTjSOp7B8rOQn68XMeGalLAUFeJs6TNktPqJRHNCKxsxPisMzBFz+DZteeJNF2xSIBor45RxS3mlnj9qkAd96mTHIZs=",
    "YyOH1fhlqrh38bnsabQXNsRw0hcMQzxmwkAOArX78tY=",
    "1ma/R67aAv5/a1RhKZfwhpFingaREa/lkGAV4PRXI/lZPLLQ3pGSoDGLz5xRRxZ+YhhTKelWd5RXGf4PVE/uOQ=="
    ]
    password = "eC9UDz6zYZ3gJaxa"
    expectedResult = ['110001010000', '555-268-5725', 'https://cloudstuff.net/article/11-14-2019/news-article', '138', 'http://example.com/?r=gakjnv&utm_source=test']
    decryptedArray = main.decryptAESEncryptedArray(array, password)
    self.assertTrue(decryptedArray == expectedResult)

  def testCategorizeMessages(self):
    messages = ['110001010000', '555-268-5725', 'https://cloudstuff.net/article/11-14-2019/news-article', '138', 'http://example.com/?r=gakjnv&utm_source=test']  
    expectedResult = {'integers': ['110001010000', '138'], 'phone numbers': ['555-268-5725'], 'URLs': ['https://cloudstuff.net/article/11-14-2019/news-article', 'http://example.com/?r=gakjnv&utm_source=test']}
    result = main.categorizeMessages(messages)
    self.assertTrue(result == expectedResult)

  def testConvertHexOrBinaryToDecimals(self):
    integers = ['0xaebfbfa', '111111111', '0xaaaa']
    expectedResult = [183237626, 511, 43690]
    integersAsDecimals = main.convertHexOrBinaryToDecimals(integers)
    self.assertTrue(integersAsDecimals == expectedResult)

  def testConvertIntegersBackToStrings(self):
    integers = [183237626, 511, 43690]
    expectedResult = ["183237626", "511", "43690"]
    integersAsStrings = main.convertIntegersBackToString(integers)
    self.assertTrue(integersAsStrings == expectedResult)

  def testCombineMessagesInResultArray(self):
    categorizedMessages = {'integers': ["511", "43690", "183237626"], 'phone numbers': ['554-000-0000', '555-258-2941'], 'URLs': ['imRunningOutOfFakeURLS.org/what', 'devopsnewstuff.net/article/blogpost/1/']}
    expectedResult = ['511', '43690', '183237626', '554-000-0000', '555-258-2941', 'imRunningOutOfFakeURLS.org/what', 'devopsnewstuff.net/article/blogpost/1/']
    result = main.combineMessagesInResultArray(categorizedMessages)
    self.assertTrue(result == expectedResult)
