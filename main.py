from Crypto.Cipher import AES
from Crypto.Util.Padding import unpad
from base64 import b64decode
from ast import literal_eval
from testcaseinputs import testCases
import re

def unobfuscatePassword(obfuscatedPassword):
    '''Obfuscated password has every 3rd letter at the  front. Every letter after is in correct order.'''
    passwordLetters = [""]*16
    password = ""
    letterIndex = 0
    for letter in obfuscatedPassword:
        #First 5 letters are every 3rd letter
        if letterIndex < 5:
            passwordLetters[letterIndex*3 + 2] = letter
            letterIndex += 1
        #Move letters to the front and skip if multiple of 3
        elif (letterIndex - 4) % 3 != 0:
            passwordLetters[letterIndex - 5] = letter
            letterIndex += 1
        #If not multiple of 3 or first five letters, move to front
        else:
            passwordLetters[letterIndex - 4] = letter
            letterIndex += 2
    for letter in passwordLetters:
        password += letter
    return password 

def decryptAESEncryptedMessage(base64Message, password):
  IV = base64Message[:16]
  decipher = AES.new(str.encode(password), AES.MODE_CBC, str.encode(IV))
  bytesMessage = unpad(decipher.decrypt(b64decode(base64Message))[16:], 16)
  stringMessage = bytesMessage.decode()
  return stringMessage

def decryptAESEncryptedArray(encryptedArray, unobfuscatedPassword):
  messages = []
  for element in encryptedArray:
    message = decryptAESEncryptedMessage(element, unobfuscatedPassword)
    messages.append(message)
  return messages

def categorizeMessages(decryptedArray):
  messages = {"integers": [], "phone numbers": [], "URLs":[]}
  numberRegex = r"^\d+$|^0[xX][0-9a-fA-F]+$"
  phoneNumberRegex = r"^((\(?\d{3})?\)( |-)|\d{3}-)\d{3}-\d{4}$"
  urlRegex = r"(https?://)?(\w|-|_|\.)+(.net|.com|.org)(\w|-|_|.|/)*?$"
  for message in decryptedArray:
    if re.match(numberRegex, message):
      messages["integers"].append(message)
    elif re.match(phoneNumberRegex, message):
      messages["phone numbers"].append(message)
    elif re.match(urlRegex, message):
      messages["URLs"].append(message)
  return messages

def convertHexOrBinaryToDecimals(arrayOfIntegers):
  newArrayOfIntegers = []
  binaryRegex = r"^(1|0)+$"
  for integer in arrayOfIntegers:
    if re.match(binaryRegex, integer):
      newArrayOfIntegers.append(int(integer, 2))
    else:
      newArrayOfIntegers.append(literal_eval(integer))
  return newArrayOfIntegers

def convertIntegersBackToString(arrayOfIntegers):
  return list(map(lambda x: str(x), arrayOfIntegers))

def organizeIntegers(arrayOfIntegers):
  decimalIntegers = convertHexOrBinaryToDecimals(arrayOfIntegers)
  decimalIntegers.sort()
  return convertIntegersBackToString(decimalIntegers)

def sortMessages(categorizedMessages):
  categorizedMessages["integers"] = organizeIntegers(categorizedMessages["integers"])
  categorizedMessages["URLs"].sort(key = lambda x: len(x))
  return categorizedMessages

def combineMessagesInResultArray(categorizedAndSortedMessages):
  return categorizedAndSortedMessages["integers"] + categorizedAndSortedMessages["phone numbers"] + categorizedAndSortedMessages["URLs"]

def decryptAndSortInput(inputObj):
  unobfuscatedPassword = unobfuscatePassword(inputObj["obfuscated password"])
  decryptedArray = decryptAESEncryptedArray(inputObj["enc inputs"], unobfuscatedPassword)
  categorizedAndSortedMessages = sortMessages(categorizeMessages(decryptedArray))
  resultArray = combineMessagesInResultArray(categorizedAndSortedMessages)
  return resultArray

def runTestCases():
  solutions = []
  for testCase in testCases:
    solutions.append(decryptAndSortInput(testCase))
  return solutions 

def writeResultsToFile(solutions):
  with open("solutions.txt", "w") as fin:
    for solution in solutions:
      solutionString = "[" + ", ".join("'" + str(x) + "'" for x in solution)
      fin.write(solutionString + "] \n\n")

if __name__ == '__main__':
  solutions = runTestCases()
  writeResultsToFile(solutions)
  