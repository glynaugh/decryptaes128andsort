import unittest
import main

class IntegrationTests(unittest.TestCase):
  def testOrganizeIntegers(self):
    arrayOfIntegers = ['0xaebfbfa', '111111111', '0xaaaa']
    expectedResult = ['511', '43690', '183237626']
    result = main.organizeIntegers(arrayOfIntegers)
    self.assertTrue(result == expectedResult)

  def testSortMessages(self):
    categorizedMessages = {'integers': ['0xaebfbfa', '111111111', '0xaaaa'], 'phone numbers': ['554-000-0000', '555-258-2941'], 'URLs': ['devopsnewstuff.net/article/blogpost/1/', 'imRunningOutOfFakeURLS.org/what']}
    expectedResult = {'integers': ['511', '43690', '183237626'], 'phone numbers': ['554-000-0000', '555-258-2941'], 'URLs': ['imRunningOutOfFakeURLS.org/what','devopsnewstuff.net/article/blogpost/1/']}
    result = main.sortMessages(categorizedMessages)
    self.assertTrue(result == expectedResult)

  def testDecryptAndSortMessages(self):
    input = {
    "enc inputs":
      [
          "kh4pOGsXY1cWiLsePjrpdbS9ZED7NKbzBiQ8GX9VrBg=",
      "sRzq2do3ANdxLaDvDO31FoHYPNnCBemJiwzxgrkhY7k=",
      "BUm/oY3Pi7yh2igwLTjSOp7B8rOQn68XMeGalLAUFeJs6TNktPqJRHNCKxsxPisMzBFz+DZteeJNF2xSIBor45RxS3mlnj9qkAd96mTHIZs=",
      "YyOH1fhlqrh38bnsabQXNsRw0hcMQzxmwkAOArX78tY=",
      "1ma/R67aAv5/a1RhKZfwhpFingaREa/lkGAV4PRXI/lZPLLQ3pGSoDGLz5xRRxZ+YhhTKelWd5RXGf4PVE/uOQ=="
      ],
    "obfuscated password": "9zYgxeCUD6zZ3Jaa"
    }
    expectedResult = ['138', '3152', '555-268-5725', 'http://example.com/?r=gakjnv&utm_source=test', 'https://cloudstuff.net/article/11-14-2019/news-article']
    result = main.decryptAndSortInput(input)
    self.assertTrue(result == expectedResult)